import React from 'react';
import PureComponent from 'react-pure-render/component';

export default class App extends PureComponent {
    render() {
        return this.props.children;
    }
}
