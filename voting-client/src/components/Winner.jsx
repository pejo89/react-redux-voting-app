import React from 'react';
import PureComponent from 'react-pure-render/component';

export default class Winner extends PureComponent {
    render() {
        return (
            <div className="winner">
                Winner is {this.props.winner}!
            </div>
        );
    }
}
